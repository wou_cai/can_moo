import moodle_api
from canvasapi import Canvas
from keys import C_URL, C_KEY, M_URL, M_KEY
import sys

canvas = Canvas(C_URL, C_KEY)
account = canvas.get_account(1)
moodle_api.URL = M_URL
moodle_api.KEY = M_KEY

terms = account.get_enrollment_terms()

sub_accounts = {}

subs = account.get_subaccounts(recursive=True)
for sub in subs:
    sub_accounts[sub.sis_account_id] = sub
    
pilot_course_list = sub_accounts['pilot'].get_courses()

users = {}
courses = {}

class User:
    def __init__(self, username):
        self.moodle_user = moodle_api.call(
            'core_user_get_users_by_field',
            field = 'username',
            values = [username]
            )[0]
        self.username = username
        self.short_email = username + '@wou.edu'
        self.long_email = username + '@mail.wou.edu'
        self.name = self.moodle_user.get('fullname')
        self.sis_id = self.moodle_user.get('idnumber')
        try:
            self.canvas_user = canvas.get_user(self.sis_id , 'sis_user_id')
        except:
            self.canvas_user = False
        users[self.username] = self

    def make_canvas_account(self):
        if not self.canvas_user:
            new_user = account.create_user(
                user = {'name': self.name},
                pseudonym = {
                    'sis_user_id': self.sis_id,
                    'unique_id': self.long_email
                    }
                )
            self.canvas_user = new_user
        return self.canvas_user
        
    def make_sanbox_course(self, account):
        self.p_course_name = self.name + ' Sandbox'
        for pilot_course in pilot_course_list:
            if pilot_course.name == self.p_course_name:
                return pilot_course
        new_course = sub_accounts['pilot'].create_course(
            course = {
                'name': self.p_course_name,
                'course_code': self.shortname,
                }
            )
        return new_course

class Course:
    def __init__(self, crn):
        self.crn = crn
        self.department = False
        self.term_id = 1
        try:
            self.moodle_course = moodle_api.call(
                'core_course_get_courses_by_field',
                field = 'idnumber',
                value = str(crn)
                )['courses'][0]
        except:
             self.moodle_course = False
        if self.moodle_course:
            self.name = self.moodle_course.get('fullname')
            self.shortname = self.moodle_course.get('shortname')
            self.term = self.moodle_course.get('categoryname')
            self.m_enrollments = moodle_api.call(
                'core_enrol_get_enrolled_users',
                courseid = self.moodle_course.get('id')
                )
            user_dict = {}
            for user in self.m_enrollments:
                role_list = []
                for role in user.get('roles'):
                    role_list.append(role.get('shortname'))
                user_dict[user.get('username')] = role_list
            self.enrollments = user_dict
        try:
            self.canvas_course = canvas.get_course(crn, use_sis_id=True)
        except:
            self.canvas_course = False
        courses[self.crn] = self
        
    def make_canvas_course(self):
        if self.department:
            sub_account = sub_accounts[self.department]
        else:
            sub_account = account
        new_course = sub_account.create_course(
            course = {
                'name': self.name,
                'course_code': self.shortname,
                'sis_course_id': self.crn,
                'enrollment_term_id': self.term_id
                }
            )
        self.canvas_course = new_course
        return new_course

    def enroll_users(self):
        for username, roles in self.enrollments.items():
            if username not in users:
                new_user = User(username)
            if not users[username].canvas_user:
                users[username].make_canvas_account()
            c_user = users[username].canvas_user
            if 'editingteacher' in roles:
                new_enrollment = self.canvas_course.enroll_user(canvas.get_user(c_user.sis_user_id, 'sis_user_id'), 'TeacherEnrollment')
                print('Teacher:', c_user.name)
            elif 'student' in roles:
                new_enrollment = self.canvas_course.enroll_user(canvas.get_user(c_user.sis_user_id, 'sis_user_id'), 'StudentEnrollment')
                print('Student:', c_user.name)
            elif roles == []:
                c_enrollments = self.canvas_course.get_enrollments()
                for enrollment in c_enrollments:
                    if enrollment.sis_user_id == idnumber:
                        enrollment.deactivate('deactivate')
