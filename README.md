# README #

### What does this do? ###

Our school is in the process of transitioning LMSs from Moodle to Canvas. For a while, we will run both systems concurrently.

This script uses the Canvas and Moodle APIs to pull data from selected courses in Moodle to set up those courses in Canvas. It uses that data to enroll students and teachers in those courses. If any of the courses or users do not exist in Canvas they will be created. This script can be run repeadedly and will only to create new users, courses or to add new students.

This works for our institution because we have an integration that creates a Moodle shell and maintains enrollments for every course with a registration number. 

### Setup ###

* Install CanvasAPI with ```pip install canvasapi```
* Add [moodle_api.py](https://github.com/mrcinv/moodle_api.py) to your working directory.
* Add your API keys and URLs to keys_example.py and rename it keys.py
* Courses are added using responses.csv which is generated using a form.

This is currently customized to our school environment. It will likely need to be tailored to fit yours.

### Who did it? ###

* haysb@mail.wou.edu
* [wou.edu/cai](wou.edu/cai)