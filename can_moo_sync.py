import can_moo
import csv
from settings import dept_dict, term_dict, current_term

courses = {}

print('Term:', current_term)

with open('responses.csv', newline='') as responses:
    reader = csv.DictReader(responses)
    for row in reader:
        if row['Term'] == current_term:
            crn = '2020' + row['Course Registration Number']
            course = can_moo.Course(crn)
            course.department = dept_dict[row['Department']]
            course.term_id = term_dict[current_term]
            courses[crn] = course
            print('CRN:',course.crn)
            print('Department:', course.department)
            if course.moodle_course:
                if not course.canvas_course:
                    course.make_canvas_course()
                course.enroll_users()
            else:
                print(crn, 'has no Moodle course.')
