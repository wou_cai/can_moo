# SETTINGS #

# Maps SIS Department Codes
dept_dict = {
    'Divination': '80DI',
    'Dark Arts': '60DA',
    'Transformation': '70TR',
    'Charms': '35CH',
    }

# Maps Term Codes
term_dict = {
    'Spring 2020': 34,
    'Summer 2020': 35,
    }

current_term = 'Summer 2020'
